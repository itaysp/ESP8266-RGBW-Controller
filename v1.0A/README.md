# ESP8266 RGBW LED Controller

Common anode rgbw led strip controller based on an ESP8266.

This is still work in progress.

![Top](https://gitlab.com/itaysp/ESP8266-RGBW-Controller/raw/master/v1.0A/TOP.png)

![Bottom](https://gitlab.com/itaysp/ESP8266-RGBW-Controller/raw/master/v1.0A/BOTTOM.png)

![Side](https://gitlab.com/itaysp/ESP8266-RGBW-Controller/raw/master/v1.0A/SIDE.png)


ESP8266 3D models and files taken from here:
https://github.com/jdunmire/kicad-ESP8266