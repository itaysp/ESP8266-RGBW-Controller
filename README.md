# ESP8266 RGBW LED Controller

Common anode rgbw led strip controller based on an ESP8266.

Version 1.0 is with AMS1117 3.3V regulator. I don't sure it won't get too  
hot, so there is now version 1.0A with an option to add a DC-DC buck  
convertor like the [DSN-MINI-360](https://www.banggood.com/Small-Mini-360-Adjustable-DC-Power-Supply-Module-Mini-Step-Down-Module-p-917568.html?cur_warehouse=CN)  
  
### UPDATE 1/12/2017 ###
I just got the PCB and soldered everything, and it works.  
Few things that I've learned:  
- The AMS1117 is getting hot, but not as I thought it would, and it operates for a few hours now.
  The copper pour did help to spread the heat.
- While the copper pour did help with the AMS1117, it did the opposite with the SHT20.
  The PCB is getting warm and it effects the SHT20 (it reads about 34C, and it's actually no more than 22C right now)
  Possible solutions:  
  - Remove the coppoer pour around the SHT20
  - Use the external buck convertor, but it might not be enought because the ESP-12F module is getting warm too
    and it's close to the SHT20
  - Change the location of the SHT20

  
![Boards](https://gitlab.com/itaysp/ESP8266-RGBW-Controller/raw/master/v1.0A/DSC_2256.jpg)
![Soldered](https://gitlab.com/itaysp/ESP8266-RGBW-Controller/raw/master/v1.0A/DSC_2270.jpg)

  
BOM
---
| Name                | Value        | Package  | Quantity  |   |  
|---------------------|:------------:|:---------:|:---------:|---|
| Capacitor           | 0.1uF        | 0603     | 4         |   |
| Capacitor           | 10uF         | 0603     | 1         |   |
| Tantalum Capacitor  | 3216         | 22uF     | 1         |   |
| Tantalum Capacitor  | 7373         | 470uF    | 1         |   |
| Schottky Diode      | 1N5817       | DO-214AC | 1         |   |
| N-Channel MOSFET    | FD8447L      | TO-252   | 4         |   |
|Resistor Array       | 10k          | 4x0603   | 1         |   |
|Resistor Array       | 100k         | 4x0603   | 1         |   |
|Resistor Array       | 330R         | 4x0603   | 1         |   |
|Resistor             | 10k          | 0603     | 3         |   |
|Linear Regulator     | AMS1117-3.3  | SOT-223  | 1         |   |
|ESP8266|ESP-12F      |              | 1        |           |   |
|Terminal Block       | KF301        | 2P       | 1         |   |
|Terminal Block       | KF301        | 5P       | 1         |   |
|Temp/Humidity Sensor | SHT20        |          | 1         | * |
|Buck Converter       | DSN-MINI-360 |          | 1         | * |
|Male Header          | 4P           | 2.54mm   | 1         |   |
|Female Header        | 1P           | 2.54mm   | 4         | * |
|Button               | SPST         | SQKG     | 1         |   |
|Power Switch         |              |          | 1         | * |

*Optional
 
**This is still work in progress.**

![Top](https://gitlab.com/itaysp/ESP8266-RGBW-Controller/raw/master/v1.0A/TOP.png)

![Bottom](https://gitlab.com/itaysp/ESP8266-RGBW-Controller/raw/master/v1.0A/BOTTOM.png)

![Side](https://gitlab.com/itaysp/ESP8266-RGBW-Controller/raw/master/v1.0A/SIDE.png)


ESP8266 3D models and files taken from here:
https://github.com/jdunmire/kicad-ESP8266