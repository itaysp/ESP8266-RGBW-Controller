#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266mDNS.h>
#include <PubSubClient.h>
#include <WiFiUdp.h>
#include <ArduinoOTA.h>


/* Wifi Settings */
const char* ssid = "YOUR_SSID";
const char* password = "YOUR_PASSWORD";

/* Device Settings */
#define DEVICE_NAME "kitchen" 

/* Mqtt Settings */
#define MQTT_SERVER "YOUR_MQTT_SERVER_ADDRESS"
#define MQTT_CLIENTUSERNAME "MQTT_CLIENT_USERNAME"
#define MQTT_CLIENTID "rgbw_strip_kitchen"
#define MQTT_CLIENTPASS "MQTT_CLIENT_PASSWORD"
#define MQTT_PORT 1883
#define MQTT_TOPIC DEVICE_NAME "/rgbw/#"
#define MQTT_TOPIC_ONOFF DEVICE_NAME "/rgbw"
#define MQTT_TOPIC_RGB DEVICE_NAME "/rgbw/RGB"
#define MQTT_TOPIC_WHITE DEVICE_NAME "/rgbw/white"
#define MQTT_TOPIC_BRIGHTNESS DEVICE_NAME "/rgbw/brightness"

/* GPIOS Definitions */
#define PIN_RED 16
#define PIN_GREEN 14
#define PIN_BLUE 12
#define PIN_WHITE 13

/* Types Definitions */
struct rgb_color
{
	uint16_t r;
	uint16_t g;
	uint16_t b;
};

/* Global Variables */
rgb_color strip_color = { 0,0,0 };
uint16_t strip_birghtness = 1023;
uint16_t strip_white = 1023;
bool isStripOn = false;
WiFiClient espClient;            
PubSubClient mqttClient(espClient);  

/* Function Prototypes */
void setStripColor(rgb_color c, uint16_t w, uint16_t brt);
rgb_color byteToRGB(byte* b, const uint8_t length);
void mqttCallback(char* topic, byte* payload, unsigned int length);
void mqttReconnect();
void handleMQTT();
void setStripColor(rgb_color c, uint16_t w, uint16_t brt);
void setStripOff();
char* byteToChar(byte* b, const uint8_t length);
uint16_t byteToNum(byte* b, const uint8_t length);
rgb_color byteToRGB(byte* b, const uint8_t length);

void setup() {
	pinMode(PIN_RED, OUTPUT);
	pinMode(PIN_GREEN, OUTPUT);
	pinMode(PIN_BLUE, OUTPUT);
	pinMode(PIN_WHITE, OUTPUT);
	setStripOff();

/* Conenct to WIFI */
	WiFi.begin(ssid, password);

	// Wait for connection
	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
	}
	Serial.println("");
	Serial.print("Connected to ");
	Serial.println(ssid);
	Serial.print("IP address: ");
	Serial.println(WiFi.localIP());

	if (MDNS.begin(MQTT_CLIENTID)) {
		Serial.println("MDNS responder started");
	}



/* Init MQTT*/
	setupMQTT();


/* Arduino OTA stuff */
	ArduinoOTA.onStart([]() {
		String type;
		if (ArduinoOTA.getCommand() == U_FLASH)
			type = "sketch";
		else // U_SPIFFS
			type = "filesystem";

		// NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
		Serial.println("Start updating " + type);
	});
	ArduinoOTA.onEnd([]() {
		Serial.println("\nEnd");
	});
	ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
		Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
	});
	ArduinoOTA.onError([](ota_error_t error) {
		Serial.printf("Error[%u]: ", error);
		if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
		else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
		else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
		else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
		else if (error == OTA_END_ERROR) Serial.println("End Failed");
	});
	ArduinoOTA.begin();
/* ^^^^^^^ End of Arduino OTA stuff ^^^^^^^^^^ */

}

// Main Loop
void loop() {
	
	handleMQTT();
	ArduinoOTA.handle();
}

//Init the MQTT client
void setupMQTT()
{
	// Mqtt Initialization
	mqttClient.setServer(MQTT_SERVER, MQTT_PORT);
	// Set Mqtt recieve callback function
	mqttClient.setCallback(mqttCallback);
}

//Handles the MQTT messages and change the led strip colors accordingly 
void mqttCallback(char* topic, byte* payload, unsigned int length) {
	//Brightness
	if (strcmp(topic, MQTT_TOPIC_BRIGHTNESS) == 0) {
		strip_birghtness = byteToNum(payload, length);
		strip_birghtness = map(strip_birghtness, 0, 255, 0, 1023);
		strip_white = 0; //disable white leds
		setStripColor(strip_color, strip_white, strip_birghtness);
	}
	

	//White
	if (strcmp(topic, MQTT_TOPIC_WHITE) == 0) {
		strip_white = byteToNum(payload, length);
		strip_white = map(strip_white, 0, 255, 0, 1023); //map value to values between 0 and 1023
		strip_color = { 0,0,0 }; //disable RGB leds
		setStripColor(strip_color, strip_white, strip_birghtness);

	}

	//RGB
	if (strcmp(topic, MQTT_TOPIC_RGB) == 0) {
		strip_color = byteToRGB(payload, length);
		strip_color.r = map(strip_color.r, 0, 255, 0, 1023); //map value to values between 0 and 1023
		strip_color.g = map(strip_color.g, 0, 255, 0, 1023); //map value to values between 0 and 1023
		strip_color.b = map(strip_color.b, 0, 255, 0, 1023); //map value to values between 0 and 1023
		strip_white = 0; //disable white 

		setStripColor(strip_color, strip_white, strip_birghtness);
	}


	//On / Off command
	if (strcmp(topic, MQTT_TOPIC_ONOFF) == 0){ 
		if (strcmp(byteToChar(payload,length), "OFF") == 0){
			setStripOff();
		}
		else 
		{
			if(isStripOn == false)
			{
				setStripColor(strip_color, strip_white, strip_birghtness); 
			}
		}
	}

}

//MQTT reconnect function
void mqttReconnect() {
	static uint8_t timeout_count = 3;
	// Loop until we're reconnected
	while (mqttClient.connected() == false) {
		if (timeout_count == 0) {
			return;
		}
		Serial.print("Attempting MQTT connection...");
		// Attempt to connect
		if (mqttClient.connect(MQTT_CLIENTID, MQTT_CLIENTUSERNAME, MQTT_CLIENTPASS)) {
			Serial.println("connected");
			// Once connected, publish an announcement...
			// ... and resubscribe
			mqttClient.subscribe(MQTT_TOPIC);

			timeout_count = 3;
		}
		else {
			timeout_count--;
			Serial.print("failed, rc=");
			Serial.print(mqttClient.state());
			Serial.println(" try again in 5 seconds");
			// Wait 5 seconds before retrying
			delay(5000);
		}
	}
}

//checks if MQTT client is conencted, if not - reconnect.
//also handles mqtt subscriptions
void handleMQTT(){
	//Check if Mqtt client disconnected
	if (mqttClient.connected() == false) {
		mqttReconnect();
	}

	//handle Mqtt
	mqttClient.loop();
}


//sets the led strip color.
//changes the RGB values according to the brighness
void setStripColor(rgb_color c, uint16_t w, uint16_t brt){
	isStripOn = true;
	//White
	analogWrite(PIN_WHITE, w);

	//rgb + brightness
	analogWrite(PIN_RED, map(c.r, 0, 1023, 0, brt)); //map value according to the brightness
	analogWrite(PIN_GREEN, map(c.g, 0, 1023, 0, brt)); //map value according to the brightness
	analogWrite(PIN_BLUE, map(c.b, 0, 1023, 0, brt)); //map value according to the brightness
}

//turn off all the leds in the srtip
void setStripOff(){
	isStripOn = false;
	digitalWrite(PIN_RED, LOW);
	digitalWrite(PIN_GREEN, LOW);
	digitalWrite(PIN_BLUE, LOW);
	digitalWrite(PIN_WHITE, LOW);
}

//converts byte* stream to char array
char* byteToChar(byte* b, const uint8_t length)
{
	char c[(length + 1)];

	for (uint8_t i = 0; i < length; i++)
	{
		c[i] = b[i];
	}
	c[length] = 0; //null therminator

	return c;
}

//converts byte* stream to nubmer
uint16_t byteToNum(byte* b, const uint8_t length)
{
	uint8_t num = 0;

	for (uint8_t  i= 0; i < length; i++)
	{
		num *= 10;
		num = num + (b[i] - '0');		
	}

	return num;
}


//Converts Byte* stream (r,g,b) to rgb_color
rgb_color byteToRGB(byte* b, const uint8_t length)
{
	rgb_color c = { 0,0,0 };
	int i = 0;

	//r
	while(b[i] != ',' && i < length)
	{
		c.r *= 10;
		c.r = c.r + (b[i] - '0');
		i++;
	}
	i++;

	//g
	while (b[i] != ',' && i < length)
	{
		c.g *= 10;
		c.g = c.g + (b[i] - '0');
		i++;
	}
	i++;

	//b
	while ( i < length)
	{
		c.b *= 10;
		c.b = c.b + (b[i] - '0');
		i++;
	}

	return c;
}