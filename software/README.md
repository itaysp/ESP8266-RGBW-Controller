# RGBW Controller Software
This is a basic software with MQTT controll on the leds.  
The SHT20 is not being used because the boards is getting warm and the readings are meaningless.

## MQTT Topics
- device_name/rgbw
  - **Message:** ON / OFF
- device_name/rgbw/RGB
  - **Message:**  r,g,b 
  - **Example:** 255,20,104
 - device_name/rgbw/white
   - **Message:** value from 0 to 255
- device_name/rgbw/brightness
   - **Message:** value from 0 to 255


## Requirements
- [ESP8266 core for Arduino](https://github.com/esp8266/Arduino)
- [Pubsubclient](https://github.com/knolleary/pubsubclient)

* If you wish to use the SHT20, I checked the SHT20 with [DFRobot_SHT20](https://github.com/DFRobot/DFRobot_SHT20) and it works.